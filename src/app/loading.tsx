import "@/components/ui/spinner/spinner.css";
import { Spinner } from "@/components/ui/spinner";

export default function Loading() {
    return <Spinner />;
}
